<?php

const defaultLatitude = 45.192492;
const defaultLongitude = 5.718117;

$result = null;
$latitude  = null;
$longitude = null;

if (!empty($_GET)) {
	$latitude  = $_GET['lat'];
	$longitude = $_GET['lon'];

	$alphabet = [];
	for ($i = 0; $i < 26; $i++) {
		$alphabet[] = chr($i + ord('A'));
	}

	$locator = [];

	$longitudeAdj = $longitude + 180;
	$latitudeAdj = $latitude + 90;

	$locator[0] = $alphabet[floor($longitudeAdj / 20)];
	$locator[1] = $alphabet[floor($latitudeAdj / 10)];

	$locator[2] = floor(($longitudeAdj / 2) % 10);
    $locator[3] = floor($latitudeAdj % 10);

    $locator[4] = strtolower($alphabet[floor(($longitudeAdj - floor($longitudeAdj / 2) * 2) * 60 / 5)]);
    $locator[5] = strtolower($alphabet[floor(($latitudeAdj - floor($latitudeAdj)) * 60 / 2.5)]);

	ksort($locator, SORT_NUMERIC);

	$result = implode('',$locator);
}
?>

<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Position GPS décimal vers locator</title>
</head>
<body>

<h1>Position GPS décimal vers locator</h1>

<?php if (!empty($locator)): ?>
	Votre locator est : <strong><?= $result ?></strong>
<?php endif; ?>

<form method="get" action="locator.php">
	Latitude : <input type="number" name="lat" step="0.000001" placeholder="<?= defaultLatitude ?>" value="<?= $latitude ?>" /><br />

	Longitude : <input type="number" name="lon" step="0.000001" placeholder="<?= defaultLongitude ?>" value="<?= $longitude ?>" /><br />

	<button type="submit">Calculer</button>
</form>

</body>
</html>

<?= highlight_file(__FILE__) ?>
