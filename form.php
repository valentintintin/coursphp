<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Formulaire</title>
</head>
<body>

<h1>Formulaire</h1>

<h2>GET</h2>

<pre><?php var_dump($_GET); ?></pre>

<form method="get" action="form.php">
	Nom : <input type="text" name="nom" /><br />
	Age : <input type="number" name="age" /><br />

	<button type="submit">Envoyer</button>
</form>

<h2>POST</h2>

<pre><?php var_dump($_POST, $_FILES); ?></pre>

<form method="post" action="form.php" enctype="multipart/form-data">
	Nom : <input type="text" name="nom" /><br />
	Age : <input type="number" name="age" /><br />
	Photo : <input type="file" name="photo" /><br />

	<button type="submit">Envoyer</button>
</form>

<h2>Server</h2>

<pre><?php var_dump($_SERVER); ?></pre>

</body>
</html>

<h2>Code source</h2>

<?= highlight_file(__FILE__) ?>
