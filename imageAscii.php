<?php

use claviska\SimpleImage;
use Spatie\Color\Rgb;

require 'vendor/autoload.php';

$ascii = '';

if (!empty($_FILES['image']['tmp_name'])) {
	$image = new SimpleImage();
	$image->fromFile($_FILES['image']['tmp_name']);

	for ($y = 0; $y < $image->getHeight(); $y ++) {
		for ($x = 0; $x < $image->getWidth(); $x ++) {
			$color = $image->getColorAt($x, $y);
			$red   = $color['red'];
			$green = $color['green'];
			$blue  = $color['blue'];

			$color = new Rgb($red, $green, $blue);
			$hsl = $color->toHsl();

			$ascii .= $hsl->lightness() >= 50 ? '-' : '0';
		}

		$ascii .= "\n";
	}
}

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Image vers ASCII</title>
</head>
<body>

<h1>Image vers ASCII</h1>

<form method="POST" action="imageAscii.php" enctype="multipart/form-data">
    <input type="file" name="image" accept="image/*" />
    <button type="submit">Afficher en ASCII</button>
</form>

<?php if ($ascii !== ''): ?>
    <pre id="ascii" style="font-size: 1px">
		<?php echo $ascii; ?>
	</pre>
<?php endif; ?>

</body>
</html>

<?= highlight_file(__FILE__) ?>
