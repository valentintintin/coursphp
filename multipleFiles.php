<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!empty($_GET['type']) && !empty($_FILES['files'])) {
	$filesUploaded = $_FILES['files'];
	$isJson = isset($_POST['json']);
	$isCompressed = isset($_POST['compress']);

	switch ($_GET['type']) {
		case 'concatener':
			$filesToConcat = [];
			$nbFiles = count($filesUploaded['name']);
			for ($i = 0; $i < $nbFiles; $i++) {
			    $fileContent = file_get_contents($filesUploaded['tmp_name'][$i]);
			    if ($isJson) {
				    $fileContent = base64_encode($fileContent);
                }
				$filesToConcat[$filesUploaded['name'][$i]] = $fileContent;
			}
			$filesContent = '';
			if ($isJson) {
				$filesContent = json_encode($filesToConcat, JSON_THROW_ON_ERROR, 512);
            } else {
			    $filesContent = serialize($filesToConcat);
            }
			if ($isCompressed) {
				$filesContent = gzcompress($filesContent, 9);
			}

			header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');
			header('Cache-Control: public'); // needed for internet explorer
			header('Content-Type: application/octet-stream');
			header('Content-Transfer-Encoding: Binary');
			header('Content-Length:' . strlen($filesContent));
			header('Content-Disposition: attachment; filename=filesConcat.bin');
            echo $filesContent;
            die;

		case 'deconcatener':
		    $filesContent = file_get_contents($filesUploaded['tmp_name']);
		    if ($isCompressed) {
			    $filesContent = gzuncompress($filesContent);
            }
		    if ($isJson) {
			    $filesToDeconcat = json_decode($filesContent, true, 512, JSON_THROW_ON_ERROR);
            } else {
			    $filesToDeconcat = unserialize($filesContent);
            }
			foreach ($filesToDeconcat as $name => $file) {
			    file_put_contents($name, $isJson ? base64_decode($file) : $file);
            }
			break;
	}
}
?>

<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Fichiers (dé)concaténateur</title>
</head>
<body>

<h1>Fichiers (dé)concaténateur</h1>

<h2>Concatener</h2>
<form method="POST" action="multipleFiles.php?type=concatener" enctype="multipart/form-data">
	<input type="file" name="files[]" multiple />
    <input type="checkbox" name="json" id="json" /><label for="json">Json</label>
    <input type="checkbox" name="compress" id="compress" /><label for="compress">Compressé</label>
	<button type="submit">Concater</button>
</form>

<h2>Déconcatener</h2>

<?php if (!empty($filesToDeconcat)): ?>
    <ul>
        <?php foreach (array_keys($filesToDeconcat) as $fileName): ?>
        <li><a href="/<?= $fileName ?>"><?= $fileName ?></a></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<form method="POST" action="multipleFiles.php?type=deconcatener" enctype="multipart/form-data">
	<input type="file" name="files" />
    <input type="checkbox" name="json" id="json2" /><label for="json2">Json</label>
    <input type="checkbox" name="compress" id="compress2" /><label for="compress2">Compressé</label>
	<button type="submit">Déconcatener</button>
</form>

</body>
</html>

<?= highlight_file(__FILE__) ?>
